export enum Status {
    CRIADA = "Criada",
    ENVIADA = "Enviada",
    CANCELADA = "Cancelada",
    PRESTACAO_CONTAS = "Prestacao de contas",
    FATURADA = "Faturada"
}