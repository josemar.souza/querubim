import { BaseModel } from './base.model';

export class EtinerarioModel {
    private destino: string;
    private partida: Date;
    private retorno: Date;
}