export class Menu {
    public title: string;
    public path: string;
    public icon: string;
    public css: string;
}