import { BaseModel } from './base.model';

export class FaturamentoModel extends BaseModel {
    private numero: string;
    private data: Date;
    private valorTotal: number;
    private valorTaxaAdministracao: number;
    private valorReembolso: number;
    private dataInicial: Date;
    private dataFinal: Date;
}