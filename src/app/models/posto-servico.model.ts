import { BaseModel } from './base.model';

export class PostoServicoModel extends BaseModel {
    public nome: string;
    public nomeResponsavel: string;
    public emailResponsavel: string;
}