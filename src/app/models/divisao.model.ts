import { BaseModel } from './base.model';

export class DivisaoModel extends BaseModel {
    public nome: string;
    public nomeResponsavel: string;
    public emailResponsavel: string;
}