import { BaseModel } from './base.model';
import { FornecedorModel } from './fornecedor.model';

export class ContratoModel extends BaseModel {
    public numero: string;
    public objeto: string;
    public valor: number;
    public fornecedor: FornecedorModel;
    public terminoVigencia: Date;
}