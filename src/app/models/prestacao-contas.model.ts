import { BaseModel } from './base.model';
import { Documento } from './documento.model';

export class PrestacaoContasModel extends BaseModel {
    private solicitacao: string;
    private data: Date;
    private motorista: string;
    private documentos: Array<Documento>;
    private valorSolicitado: number;
    private valorComprovado: number;
    private valorMaximo: number;
    private valorExcedido: number;
    private valorDevolver: number;
    private valorReembolso: number;
    private observacao: string;

    private solicitacaoId: string;
}