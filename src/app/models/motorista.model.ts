import { BaseModel } from './base.model';
import { PostoServicoModel } from './posto-servico.model';

export class MotoristaModel extends BaseModel {
    public nome: string;
    public posto: PostoServicoModel;
    public cnh: string;
    public validadeCNH: Date;
}