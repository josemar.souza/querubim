export class FornecedorModel {
    public nome: string;
    public cnpj: string;
    public endereco: string;
    public telefone: string;
    public email: string;
    public logoUrl: string;
}