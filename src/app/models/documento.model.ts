import { TipoDocumento } from './tipo-documento.enum';

export class Documento {
    private numero: string;
    private data: Date;
    private tipo: TipoDocumento;
    private localidade: string;
    private valor: number;
    private observacao: string;
}