import { BaseModel } from './base.model';
import { Status } from './status.enum';
import { EtinerarioModel } from './etinerario.model';

export class SolicitacaoModel extends BaseModel {
    public numero: string;
    public data: Date;
    public motorista: string;
    public valorTotal: number;
    public status: Status;
    public observacao: string;
    public diariasComPernoite: number;
    public diariasSemPernoite: number;
    public etinerarios: Array<EtinerarioModel>;
}