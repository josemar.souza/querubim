export class BaseModel {
    public id: string;
    public toPlainObject(): {} {
        return Object.assign({}, this);
    }
}