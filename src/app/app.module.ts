import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { CadastrosComponent } from './components/cadastros/cadastros.component';
import { HomeComponent } from './components/home.component';
import { MotoristaComponent } from './components/cadastros/motorista/motorista.component';
import { SidebarComponent } from './components/layout/sidebar/sidebar.component';
import { NavbarComponent } from './components/layout/navbar/navbar.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { BreadcrumbComponent } from './components/layout/breadcrumb/breadcrumb.component';
import { OperacionalComponent } from './components/operacional/operacional.component';
import { ConfiguracoesComponent } from './components/configuracoes/configuracoes.component';
import { ContratoComponent } from './components/cadastros/contrato/contrato.component';
import { ToastrModule } from 'ngx-toastr';
import { LoadingBarModule } from '@ngx-loading-bar/core';

@NgModule({
  declarations: [
    AppComponent,
    CadastrosComponent,
    HomeComponent,
    MotoristaComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    BreadcrumbComponent,
    OperacionalComponent,
    ConfiguracoesComponent,
    ContratoComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    LoadingBarModule
  ],
  providers: [AngularFirestore, AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
