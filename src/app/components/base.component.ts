import { BaseModel } from '../models/base.model';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';

export abstract class BaseComponent<T extends BaseModel> {
    public listaModel: Observable<T[]>;
    public exibeNovo = false;
    public formModel: FormGroup;
    constructor() {

    }
    abstract initForm(item: T) ;
    abstract async onSubmit();
    editar(item: T) {
        this.initForm(item);
        this.exibeNovo = true;
    }
    alternar() {
        this.exibeNovo = !this.exibeNovo;
    }
}