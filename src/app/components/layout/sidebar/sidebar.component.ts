import { Component, OnInit } from '@angular/core';
import { Menu } from 'src/app/models/menu.model';

@Component({
  selector: 'sidebar-cmp',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  public menus: Array<Menu>;
  constructor() {
    this.menus = new Array<Menu>();
    const menu = new Menu();
    menu.css='';
    menu.icon='';
    menu.path='cadastros';
    menu.title='Cadastros';
    this.menus.push(menu);
    this.menus.push(menu);
    this.menus.push(menu);
    this.menus.push(menu);
   }

  ngOnInit(): void {
    
  }

}
