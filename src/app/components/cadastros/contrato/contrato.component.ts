import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base.component';
import { ContratoModel } from 'src/app/models/contrato.model';
import { ContratoService } from 'src/app/services/contrato.service';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FornecedorModel } from 'src/app/models/fornecedor.model';
import { NotificacaoService, TipoNotificacao } from 'src/app/services/notificacao.service';
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'app-contrato',
  templateUrl: './contrato.component.html',
  styleUrls: ['./contrato.component.css']
})
export class ContratoComponent extends BaseComponent<ContratoModel> implements OnInit {
 
  constructor(
    private contratoService: ContratoService, 
    private notificacao: NotificacaoService,
    private loadingBar: LoadingBarService) {
    super();
  }

  ngOnInit(): void {
    this.listaModel = this.contratoService.list();
    this.initForm(new ContratoModel());
  }
  initForm(item: ContratoModel) {
    if(!item.fornecedor) {
      item.fornecedor = new FornecedorModel();
    }
    this.formModel = new FormGroup({
      id: new FormControl(item.id),
      numero: new FormControl(item.numero, [Validators.required, Validators.minLength(10)]),
      objeto: new FormControl(item.objeto, [Validators.required]),
      valor: new FormControl(item.valor, [Validators.required]),
      terminoVigencia: new FormControl(item.terminoVigencia),
      fornecedorNome: new FormControl(item.fornecedor.nome, [Validators.required]),
      fornecedorCnpj: new FormControl(item.fornecedor.cnpj, [Validators.required]),
      fornecedorEndereco: new FormControl(item.fornecedor.endereco, [Validators.required]),
      fornecedorTelefone: new FormControl(item.fornecedor.telefone, [Validators.required]),
      fornecedorEmail: new FormControl(item.fornecedor.email, [Validators.required]),
      fornecedorLogoUrl: new FormControl(item.fornecedor.logoUrl)
    }, { updateOn: 'blur' });
  }
  async onSubmit() {
    const _self = this;
    try {
      if (!this.formModel.valid) {
        throw new Error('Informe todos os campos obrigatórios');
      }
      const contrato: ContratoModel = new ContratoModel();
      contrato.id = this.formModel.controls['id'].value;
      contrato.numero = this.formModel.controls['numero'].value;
      contrato.objeto = this.formModel.controls['objeto'].value;
      contrato.valor = this.formModel.controls['valor'].value;
      contrato.terminoVigencia = this.formModel.controls['terminoVigencia'].value;
      const fornecedor = new FornecedorModel();
      fornecedor.nome = this.formModel.controls['fornecedorNome'].value;
      fornecedor.cnpj = this.formModel.controls['fornecedorCnpj'].value;
      fornecedor.endereco = this.formModel.controls['fornecedorEndereco'].value;
      fornecedor.telefone = this.formModel.controls['fornecedorTelefone'].value;
      fornecedor.email = this.formModel.controls['fornecedorEmailj'].value;
      fornecedor.logoUrl = this.formModel.controls['fornecedorLooUrl'].value;
      contrato.fornecedor = fornecedor;
      await this.contratoService.salvar(contrato)
        .then(async function() {
          _self.ngOnInit();
          _self.notificacao.showNotification(TipoNotificacao.SUCCESS, 'Salvo com sucesso!');
        })
        .catch(function(error) {
          throw error;
        });
    } catch (error) {
      if (error.name === 'validation-error') {
        this.notificacao.showNotification(TipoNotificacao.WARNING, error.message);
      } else {
        this.notificacao.showNotification(TipoNotificacao.ERROR, error.message);
      }
    } finally {
      this.loadingBar.stop();
    }
  }



}
