import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base.component';
import { MotoristaModel } from 'src/app/models/motorista.model';

@Component({
  selector: 'app-motorista',
  templateUrl: './motorista.component.html',
  styleUrls: ['./motorista.component.css']
})
export class MotoristaComponent extends BaseComponent<MotoristaModel> implements OnInit {

  constructor() { 
    super();
  }

  ngOnInit(): void {
  }

  initForm(item: MotoristaModel) {

  }
  async onSubmit() {

  }
}
