import { Component, OnInit } from '@angular/core';
import { SolicitacaoModel } from './models/solicitacao.model';
import { Status } from './models/status.enum';
import { SolicitacaoService } from './services/solicitacao.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'querubim';
  constructor(private solicitacaoService: SolicitacaoService) { }
  ngOnInit(): void {
    const solicitacao = new SolicitacaoModel();
    solicitacao.data = new Date();
    solicitacao.motorista = "JOAO DA SILVA";
    solicitacao.status = Status.CRIADA;
    solicitacao.diariasComPernoite = 1;
    solicitacao.diariasSemPernoite = 1;
    solicitacao.numero = "001/2020";
    solicitacao.valorTotal = 185.00;
    this.solicitacaoService.add(solicitacao);
  }

}
