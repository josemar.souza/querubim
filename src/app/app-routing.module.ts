import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home.component';
import { CadastrosComponent } from './components/cadastros/cadastros.component';
import { MotoristaComponent } from './components/cadastros/motorista/motorista.component';
import { OperacionalComponent } from './components/operacional/operacional.component';
import { ConfiguracoesComponent } from './components/configuracoes/configuracoes.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'cadastros', 
    children: [
      {
          path: '',
          component: CadastrosComponent
      },
      {
          path: 'motorista',
          component: MotoristaComponent
      }
    ]    
  },
  { path: 'operacional', 
    children: [
      {
          path: '',
          component: OperacionalComponent
      }
    ]    
  },
  { path: 'configuracoes', 
    children: [
      {
          path: '',
          component: ConfiguracoesComponent
      }
    ]    
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
