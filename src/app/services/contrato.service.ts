import { FirestoreService } from './firestore.service';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ContratoModel } from '../models/contrato.model';

@Injectable({providedIn: 'root'})
export class ContratoService extends FirestoreService<ContratoModel> {
    constructor(private firestore: AngularFirestore) {
        super(firestore, 'contratos');
    }

    public salvar(contrato: ContratoModel): Promise<any> {
        if(!contrato.id) {
            contrato.id = contrato.numero;
            return this.add(contrato)
        }else {
            return this.update(contrato);
        }

    }
}