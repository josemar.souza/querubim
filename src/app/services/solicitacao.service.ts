import { FirestoreService } from './firestore.service';
import { Injectable } from '@angular/core';
import { SolicitacaoModel } from '../models/solicitacao.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({providedIn: 'root'})
export class SolicitacaoService extends FirestoreService<SolicitacaoModel> {
    constructor(private firestore: AngularFirestore) {
        super(firestore, 'contratos/45001230041/solicitacoes');
    }
}